import Foundation
import Kitura
import KituraStencil
import KituraSession

struct Message : Codable {
	let author: String
	let msg: String
  let date: String
}

func dateDuJour() -> String {
  let now = Date()
  let dateFormat = DateFormatter()
  dateFormat.dateStyle = .short
  dateFormat.timeStyle = .short
  dateFormat.locale = Locale(identifier: "FR-fr")
  return dateFormat.string(from: now)
}

let router = Router()
var history : [Message] = []
let session = Session(secret: "secret")

router.add(templateEngine: StencilTemplateEngine())
router.all(middleware: [BodyParser(), StaticFileServer(path: "./Public")])
router.all(middleware: Session(secret: "secret"))

router.get("/") { request, response, next in
  try response.render("index.stencil", context: ["messages": history])
  next()
}

router.post("/") { request, response, next in
  if let body = request.body?.asURLEncoded, let msg = body["msg"], let author = body["author"] {
    if msg.lengthOfBytes(using: .utf8) == 0 {
      try response.render("index.stencil", context: ["messages": history])
      } else {
        history.append(Message(author: author, msg: msg, date: dateDuJour()));
        try response.render("index.stencil", context: ["messages": history])
      }
    } else {
      try response.redirect("/").end()
   }
  next()
}

Kitura.addHTTPServer(onPort: 8080, with: router)
Kitura.run()
